User Guide
==============

Overview
--------

The PMnet Platform consists of

- A `Workflow Repository <https://gitlab.repository.oncopmnet.gr/pipelines>`_.
- A `Tools Repository <https://gitlab.com/precmed/tools>`_.
- A *Workflow Execution Service* based on `Cromwell <https://cromwell.readthedocs.io/>`_.
- A *Storage Service* that implements the S3 protocol and can be browsed using
  `Minio Browser <https://min.io/>`_.

Edit your workflow
------------------

Typically, a workflow description (typically written in `CWL <https://www.commonwl.org/>`_)
reside in the dedicated code repository of the platform. The repository is based
on `Gitlab <https://gitlab.com/gitlab-org/gitlab-ce/>`_ and
supports versioning control via `Git <https://git-scm.com/>`_, issue tracking and
discussion and continuous integration and delivery (CI/CD).

This tutorial assumes familiarity with the typical code development lifecycle.

The current incarnation of the PMnet Workflow Repository can be accessed at
`<https://gitlab.repository.oncopmnet.gr/>`_. In order to use this repository,
you should:

1. create an account,
2. create a new repository in your account,
3. commit your CWL workflow and CWL tool descriptions.

.. Note::
   At this stage you must commit at a public repository.

A simple definition of a two-step CWL workflow definition can be viewed
`here <https://gitlab.repository.oncopmnet.gr/pipelines/demo/-/raw/master/Simple-Workflow/simple-workflow.cwl>`_.

Upload the inputs files
-----------------------

Use `Minio Browser <https://min.io/>`_ to upload the inputs of your workflow. For these tests upload
your files inside the bucket called ``workflow-inputs``. To avoid filename conflicts
please use a characteristic filename, e.g. ``ncsr_myfile_1.fastq.gz``. Once a file is uploaded, it can be used again in the future without having to reupload it. 



Create the input description
----------------------------

You need to create an input description file in order to continue with the
submission of your workflow. An input description file contains all the inputs
needed by your workflow and is expressed in `YAML <https://yaml.org/>`_. Input
files are identified by their URLs and you need to construct each URL of the
files you uploaded in the aforementioned step. An input description file will
look similar to the following:

.. code-block:: yaml

    read1:
      class: File
      path: s3://internal.s3.endpoint/workflow-inputs/ncsr_myfile_1.fastq.gz
    read2:
      class: File
      path: s3://internal.s3.endpoint/workflow-inputs/ncsr_myfile_2.fastq.gz
    sample: "15tu_S1"
    rg_name: "G13GV.1"
    seq_center: "MLCDiag"


``internal.s3.endpoint`` will be replaced by the system with the real internal s3 endpoint upon submission. Users should follow this convention for their URLs, so every file must follow pattern ``s3://internal.s3.endpoint/bucket-name/path/to/file``.

When ready, save the created input description file locally.

Submit your workflow
--------------------

At this stage, the PMnet platform exposes the RESTful API of the *Workflow Execution Service*
that include functionality for workflow submission. The capabilities of the API
are documented in its `Swagger manifest <https://gitlab.com/precmed/deployment/rest-api/-/blob/master/README.md>`_.

For the workflow submission you will need the following terminal command that uses
``cURL``.

Using the following command, you will retrieve your access token.

.. code-block:: bash

    ACCESS_TOKEN=$(curl -d 'client_id=service-rest' -d 'client_secret=<CLIENT_SECRET>' -d 'username=<USERNAME>' -d 'password=<PASSWORD>' -d 'grant_type=password' 'https://accounts-test.oncopmnet.gr/auth/realms/precmed/protocol/openid-connect/token' | grep -oP '(?<="access_token":")[^"]*')

You can find the ``CLIENT_SECRET`` of service-rest from keycloak or k8s and is the one set in installation.


In order to launch a custom workflow you must run the following command:

.. code-block:: bash

   curl -v -X POST -H "Authorization: Bearer $ACCESS_TOKEN" "https://rest.service-test.oncopmnet.gr/launch" \
        -F type=custom \ 
        -F workflowUrl=https://gitlab.repository.oncopmnet.gr/pipelines/demo/-/raw/master/Simple-Workflow/simple-workflow.cwl 
        -F inputs_s3=simple-workflow.yml



The ``workflowUrl`` is the URL of your workflow, for example ``https://gitlab.repository.oncopmnet.gr/pipelines/demo/-/raw/master/Simple-Workflow/simple-workflow.cwl``. Please note that when submitting a file from gitlab you must always use the raw representation 
the file.

In order to launch a predefined workflow you must run the following command:

.. code-block:: bash

    curl -v -X POST -H "Authorization: Bearer $ACCESS_TOKEN" ""https://rest.service-test.oncopmnet.gr/launch" \
           -F orderId=<myOrderID> \
           -F type=<workflowType> \ 
           -F <workflowType>-<inputType>=@<inputFile>

If you want to submit a workflow with type ``Solid tumors (Illumina)`` or ``Hematologic malignancies (Illumina)`` or ``Hereditary-cancer`` you need to extend previous command by adding:

.. code-block:: bash
     
   -F <workflowType>-<inputType2>=@<inputFile2> 

For each ``workflowType``, ``inputFile`` must have specific ``inputType`` as shown in the next table.


+---------------------------------------+------------------+
| workflowType                          | inputType        |
+=======================================+==================+
| somatic-illumina-solid                | fastq1 and fastq2|
+---------------------------------------+------------------+
|somatic-torrent-solid                  | bam              |
+---------------------------------------+------------------+
| somatic-illumina-hematologic          |fastq1 and fastq2 |
+---------------------------------------+------------------+
| somatic-torrent-hematologic           | bam              |
+---------------------------------------+------------------+
| hereditary-cancer                     |fastq1 and fastq2 |
+---------------------------------------+------------------+

The ``myOrderId`` parameter needs to be unique and in the form of ``XXXX-XXXXX-XXXX``, where ``X`` a digit.

The ``workflowInputs`` is the path for your input description file that reside locally.
The other parameters should remain fixed since you are submitting a CWL workflow.

Upon submission you will see the ``id`` in your terminal. This id can be used
to inspect the progress as follows:

.. code-block:: bash

   curl -v -X POST -H "Authorization: Bearer $ACCESS_TOKEN" "https://rest.service-test.oncopmnet.gr/status" -F orderId=<myOrderId>

In case a workflow fails, you will notice `status:Failed` in the response you get. If a workflow failed, you will need to launch a new workflow by running the same command as before. The input files that were used before are still available so you don't have to change anything in your command.

To get the URLs to the outputs as follows

.. code-block:: bash

  curl -v -X POST -H "Authorization: Bearer $ACCESS_TOKEN" "https://rest.service-test.oncopmnet.gr/outputs" -F orderId=<myOrderId>

Outputs can also be inspected using `VAST Annotation Tool <put something here>`_

Note that you can also submit your workflows using `Precmed UI <https://gitlab.com/precmed/deployment/web-ui/>`_, by uploading the appropriate files  

Get the results
---------------

Upon completion of your workflow you can access the *Storage Service* using
the `Minio Browser <https://min.io/>`_ and navigate inside the ``workflow-runs`` bucket. The bucket
is organized first by the workflow CWL filename and then by the ``id``.
Inside you will find a folder structure of all intermediate and final outputs.

Operational Metadata
--------------------
You can inspect metadata of a workflow as follows

.. code-block:: bash

   curl -v -X POST -H "Authorization: Bearer $ACCESS_TOKEN" "https://rest.service-test.oncopmnet.gr/metadata" -F orderId=<myOrderId>

``ACCESS_TOKEN`` can be retrieved as described before.

You can also have access to metadata through `Precmed metadata <https://service-test.oncopmnet.gr/metadata/>`_, by providing your desired `orderId`
