.. PMnet Platform documentation master file, created by
   sphinx-quickstart on Wed Mar  6 18:55:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
